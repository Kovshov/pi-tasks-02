#include <stdio.h>
#include <time.h>
#include <locale.h>

int getRandomInt()
{
	int r;
	r = 48 + rand() % 75; // 48-����������� ����� ������� ����� �������� � ���������� �������. 48+74 - ������������
	if (r >=':' && r <='@' || r >='[' && r <='`') //���� ��������, ������� �� ������ �������������� � ������, ������� �� �� ����� �������������
	{
		getRandomInt();
	} else 
	{
		return r;
	}
}

int main()
{
	setlocale(LC_ALL, "Rus");
	srand(time(NULL));
	
	int N, i, j;
	char symb;
	
	printf("������� ������ ������: ");
	scanf("%d", &N);
	
	for(j = 0; j < 40; j++){
		for(i = 0; i < N; i++){
			symb = getRandomInt();
			if(i == N - 1){
				printf("%c   ", symb);	
			} else{
				printf("%c", symb);
			}
		}
		if(j % 2)
		{
			printf("\n");
		}
	}
	
	return 0;
}
